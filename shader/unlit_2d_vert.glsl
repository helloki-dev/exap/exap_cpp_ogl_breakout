#version 330 core

// <vec2 pos, vec2 tex_coords>
layout (location = 0) in vec4 vertex;

out vec2 tex_coords;

uniform mat4 model;
uniform mat4 proj;

void main()
{
    tex_coords = vertex.zw;
    gl_Position = proj * model * vec4(vertex.xy, 0.0, 1.0);
}
