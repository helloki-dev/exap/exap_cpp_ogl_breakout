#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "game/game.h"
#include "assets/asset_manager.h"



void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void key_callback(GLFWwindow *window, int key, int scancode, int action, int mode);



const unsigned int SCREEN_WIDTH = 800;
const unsigned int SCREEN_HEIGHT = 600;

hll::game::GameData *game_data;



int main()
{
    std::cout << "Hellengine started!" << std::endl;

    // Init GLFW
    // ---------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    glfwWindowHint(GLFW_RESIZABLE, false);

    GLFWwindow *window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hellengine", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // Init GLEW
    // ---------
    GLenum glew_err = glewInit();
    if (glew_err != GLEW_OK) { std::cout << "Failed to initialize glew!"; }

    glfwSetKeyCallback(window, key_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // Configure OpenGL
    // ----------------
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Init game
    // ---------
    game_data = hll::game::create(SCREEN_WIDTH, SCREEN_HEIGHT);

    float delta_time = 0.0f;
    float last_frame = 0.0f;

    while (!glfwWindowShouldClose(window))
    {
        float curr_frame = glfwGetTime();
        delta_time = curr_frame - last_frame;
        last_frame = curr_frame;

        glfwPollEvents();

        // Process
        // -------
        hll::game::process_input(*game_data, delta_time);
        hll::game::update(delta_time, *game_data);

        // Render
        // ------
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        hll::game::render(*game_data);

        glfwSwapBuffers(window);
    }

    // Clean up
    // --------
    hll::AssetManager::clear();
    glfwTerminate();

    std::cout << "Hellengine stopped!" << std::endl;
    return 0;
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mode)
{
    if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)) {
        glfwSetWindowShouldClose(window, true);
    }

    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
        {
            game_data->keys[key] = true;
        }
        else if (action == GLFW_RELEASE)
        {
            game_data->keys[key] = false;
        }
    }
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}
