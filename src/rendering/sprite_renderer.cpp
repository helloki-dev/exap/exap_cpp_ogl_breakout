#include <GL/glew.h>

#include "sprite_renderer.h"



const auto U_MODEL = "model";
const auto U_SPRITE_COLOR= "sprite_color";



hll::sprite_renderer::RenderInfo hll::sprite_renderer::create_data(unsigned int shader)
{
    const float verts[] = {
            // pos      // tex
            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,

            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f
    };

    unsigned int vao, vbo;

    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, (4 * sizeof(float)), (void*)0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return hll::sprite_renderer::RenderInfo{
        shader,
        vao,
    };
}

void hll::sprite_renderer::delete_data(unsigned int vao)
{
    glDeleteVertexArrays(1, &vao);
}

void hll::sprite_renderer::draw_sprite(const RenderInfo &info, unsigned int tex, glm::vec2 pos, glm::vec2 size, float rot_deg, glm::vec3 color)
{
    glm::mat4 model = glm::mat4(1.0f);
    // Translate
    model = glm::translate(model, glm::vec3(pos, 0.0f));
    // Rotate (around center)
    model = glm::translate(model, glm::vec3((0.5f * size.x), (0.5f * size.y), 0.0f));
    model = glm::rotate(model, glm::radians(rot_deg), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model, glm::vec3((-0.5f * size.x), (-0.5f * size.y), 0.0f));
    // Scale
    model = glm::scale(model, glm::vec3(size, 1.0f));

    hll::shader::use(info.shader);
    hll::shader::set_mat_4f(info.shader, U_MODEL, model);
    hll::shader::set_vec_3f(info.shader, U_SPRITE_COLOR, color);

    glActiveTexture(GL_TEXTURE0);
    hll::tex_2d::bind(tex);

    glBindVertexArray(info.vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}