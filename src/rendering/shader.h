#ifndef HELLENGINE_SHADER_H
#define HELLENGINE_SHADER_H

#include <string>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace  hll::shader {
    void use(unsigned int shader);
    unsigned int compile(const char *vert_src, const char *frag_src, const char *geo_src);
    void check_compile_errors(unsigned int obj, const std::string &type);

    void set_float(unsigned int shader, const char *name, float val);
    void set_int(unsigned int shader, const char *name, int val);
    void set_vec_2f(unsigned int shader, const char *name, float x, float y);
    void set_vec_2f(unsigned int shader, const char *name, const glm::vec2 &val);
    void set_vec_3f(unsigned int shader, const char *name, float x, float y, float z);
    void set_vec_3f(unsigned int shader, const char *name, const glm::vec3 &val);
    void set_vec_4f(unsigned int shader, const char *name, float x, float y, float z, float w);
    void set_vec_4f(unsigned int shader, const char *name, const glm::vec4 &val);
    void set_mat_4f(unsigned int shader, const char *name, glm::mat4 &val);
}


#endif //HELLENGINE_SHADER_H
