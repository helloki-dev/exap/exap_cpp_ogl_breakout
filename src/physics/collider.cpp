
#include <tuple>
#include <iostream>

#include "collider.h"

bool hll::collider::check_collision_2d_aabb_aabb(glm::vec2 obj_1_tl, glm::vec2 obj_1_br, glm::vec2 obj_2_tl, glm::vec2 obj_2_br) {
    const bool col_x = (obj_1_br.x >= obj_2_tl.x) && (obj_2_br.x >= obj_1_tl.x);
    const bool col_y = (obj_1_br.y >= obj_2_tl.y) && (obj_2_br.y >= obj_1_tl.y);

    return col_x && col_y;
}

hll::collider::CollisionInfo hll::collider::check_collision_2d_circle_aabb(glm::vec2 cir_center, float cir_radius, glm::vec2 box_tl, glm::vec2 box_size) {
    // calculate box center and half_extends
    glm::vec2 aabb_half_extents((box_size.x / 2.0f), (box_size.y / 2.0f));
    glm::vec2 box_center((box_tl.x + aabb_half_extents.x), (box_tl.y + aabb_half_extents.y));

    // determine point on aabb that is closest to the circle
    glm::vec2 diff = cir_center - box_center;
    glm::vec2 clamped = glm::clamp(diff, -aabb_half_extents, aabb_half_extents);
    glm::vec2 closest = box_center + clamped;

    // check distance to closest point
    diff = closest - cir_center;

    if (glm::length(diff) < cir_radius) {
        return std::make_tuple(true, hll::collider::calc_vec_direction(diff), diff);
    } else {
        return std::make_tuple(false, hll::collider::Direction::UP, glm::vec2(0.0f, 0.0f));
    }
}

hll::collider::Direction hll::collider::calc_vec_direction(glm::vec2 target) {
    const glm::vec2 compass[] {
            glm::vec2(0.0f, 1.0f),      // up
            glm::vec2(0.0f, -1.0f),     // down
            glm::vec2(-1.0f, 0.0f),     // left
            glm::vec2(1.0f, 0.0f),      // right
    };

    float max_dot = 0.0f;
    unsigned int best_index = -1;

    for (unsigned int i = 0; i < 4; i++) {
        float dot = glm::dot(glm::normalize(target), compass[i]);

        if (dot > max_dot) {
            max_dot = dot;
            best_index = i;
        }
    }

    return static_cast<hll::collider::Direction>(best_index);
}
