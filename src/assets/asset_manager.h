
#ifndef HELLENGINE_ASSET_MANAGER_H
#define HELLENGINE_ASSET_MANAGER_H

#include <string>
#include <map>
#include <utility>

#include "../rendering/texture_2d.h"

namespace hll {
    class AssetManager
    {
    public:
        static std::map<std::string, unsigned int> shaders;
        static std::map<std::string, unsigned int> textures;
        static std::map<std::string, hll::tex_2d::TexInfo> texture_infos;

    public:
        static unsigned int load_shader(const char *vert_file_path, const char *frag_file_path, const char *geo_file_path, std::string name);
        static unsigned int get_shader(std::string name);
        static hll::tex_2d::TexInfo load_tex_2d(const char *file_path, bool alpha, std::string name);
        static unsigned int get_tex_2d(std::string name);
        static bool tex_2d_is_loaded(std::string name);
        static hll::tex_2d::TexInfo get_tex_2d_info(std::string name);
        static void clear();

        static bool is_valid_path(const char* file_path);

    private:
        AssetManager();
        static unsigned int load_shader_from_file(const char *vert_file_path, const char *frag_file_path, const char *geo_file_path);
        static std::pair<unsigned int, hll::tex_2d::TexInfo> load_tex_2d_from_file(const char *file, bool alpha);
    };
}

#endif //HELLENGINE_ASSET_MANAGER_H
