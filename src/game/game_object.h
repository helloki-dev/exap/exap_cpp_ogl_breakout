
#ifndef HELLENGINE_GAME_OBJECT_H
#define HELLENGINE_GAME_OBJECT_H

#include <glm/glm.hpp>
#include "../rendering/sprite_renderer.h"
#include "transform.h"

namespace hll {
    class GameObject {
    public:
        glm::vec2 pos, size, vel;
        glm::vec3 color;
        float rot;
        bool is_solid;
        bool is_destroyed;
        unsigned int tex;

    public:
        GameObject();
        GameObject(glm::vec2 pos, glm::vec2 size, unsigned int tex, glm::vec3 color, glm::vec2 vel);

        virtual void draw(const sprite_renderer::RenderInfo &render_info) const;
    };
}

#endif //HELLENGINE_GAME_OBJECT_H

