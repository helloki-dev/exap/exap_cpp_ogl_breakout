#ifndef HELLENGINE_GAME_LEVEL_H
#define HELLENGINE_GAME_LEVEL_H

#include <vector>

#include "game_object.h"


namespace hll {

    class GameLevel {
    public:
        std::vector<hll::GameObject> bricks;
        const char *file_path;
        unsigned int height, width;

    public:
        GameLevel();
        void load(const char *file_path, unsigned int level_width, unsigned int level_height);
        void reload();
        void draw(const hll::sprite_renderer::RenderInfo &render_info) const;
        bool is_completed();

    private:
        void init(std::vector<std::vector<unsigned int>> tile_data);
    };

    void test(float a, int b);
}



#endif //HELLENGINE_GAME_LEVEL_H
