#ifndef HELLENGINE_GAME_H
#define HELLENGINE_GAME_H

#include <glm/glm.hpp>

#include "game_level.h"

namespace hll::game {
const auto TMP_SPRITE_NAME = "sprite";

const auto TMP_VERT_FILE_PATH = "../shader/unlit_2d_vert.glsl";
const auto TMP_FRAG_FILE_PATH = "../shader/unlit_2d_frag.glsl";

const auto BACKGROUND_PATH = "../assets/pkgs/_com/tex/background.jpg";
const auto AWESOME_FACE_PATH = "../assets/pkgs/_com/tex/awesomeface.png";
const auto BLOCK_PATH = "../assets/pkgs/_com/tex/block.png";
const auto BLOCK_SOLID_PATH = "../assets/pkgs/_com/tex/block_solid.png";
const auto PADDLE_PATH = "../assets/pkgs/_com/tex/paddle.png";

const auto BACKGROUND_NAME = "background";
const auto AWESOME_FACE_NAME = "face";
const auto BLOCK_NAME = "block";
const auto BLOCK_SOLID_NAME = "block_solid";
const auto PADDLE_NAME = "paddle";


const auto LVL_0_PATH = "../assets/pkgs/lvl_1/lvl_1.scene";
const auto LVL_1_PATH = "../assets/pkgs/lvl_2/lvl_2.scene";
const auto LVL_2_PATH = "../assets/pkgs/lvl_3/lvl_3.scene";
const auto LVL_3_PATH = "../assets/pkgs/lvl_4/lvl_4.scene";

const auto U_PROJ = "proj";
const auto U_IMG = "img";

const glm::vec2 PLAYER_SIZE(100.0f, 20.0f);
const float PLAYER_SPEED(500.0f);

const glm::vec2 INITIAL_BALL_VELOCITY(100.0f, -350.0f);
const float BALL_RADIUS = 12.5f;
}

namespace hll::game {

enum class GameState {
    GAME_ACTIVE,
    GAME_MENU,
    GAME_WIN
};

struct GameData {
public:
    GameState state;
    bool keys[1024];
    unsigned int width, height;

    std::vector<hll::GameLevel> levels;
    unsigned int curr_level;

    hll::sprite_renderer::RenderInfo render_info;

public:
    GameLevel &get_curr_lvl() const;
};

GameData *create(unsigned int width, unsigned int height);
void process_input(const hll::game::GameData &game_data, float dt);
void update(float dt, GameData &game_data);
void render(const hll::game::GameData &game_data);
void handle_collisions(std::vector<GameObject> &brick_objs);

void reset_lvl(GameLevel &curr_lvl);
void reset_ply(unsigned int width, unsigned int height);

}


#endif //HELLENGINE_GAME_H
