
#ifndef HELLENGINE_TRANSFORM_H
#define HELLENGINE_TRANSFORM_H

#include <glm/glm.hpp>


namespace hll {
    struct Transform {
        glm::vec3 position;
        glm::vec3 size;
        float rotation;
    };
}


#endif //HELLENGINE_TRANSFORM_H
