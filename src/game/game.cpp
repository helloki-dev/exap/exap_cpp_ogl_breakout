#include <iostream>
#include <ranges>

#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

#include "game.h"
#include "../assets/asset_manager.h"
#include "../rendering/shader.h"
#include "../rendering/sprite_renderer.h"
#include "ball_object.h"
#include "../physics/collider.h"


hll::GameObject *player_obj;
hll::BallObject *ball_obj;



// ================================================================================================
// struct GameData
// ================================================================================================

hll::GameLevel& hll::game::GameData::get_curr_lvl() const {
    this->levels[this->curr_level];
}

// ================================================================================================
// Other
// ================================================================================================

hll::game::GameData *hll::game::create(unsigned int width, unsigned int height) {
    const auto state = hll::game::GameState::GAME_ACTIVE;
    const auto near = -1.0f;
    const auto far = 10.0f;

    // Init shader
    // -----------
    glm::mat4 proj = glm::ortho(0.0f, static_cast<float>(width), static_cast<float>(height), 0.0f, near, far);

    const auto sprite_shader = hll::AssetManager::load_shader(
            TMP_VERT_FILE_PATH,
            TMP_FRAG_FILE_PATH,
            nullptr,
            TMP_SPRITE_NAME);

    hll::shader::use(sprite_shader);
    hll::shader::set_int(sprite_shader, U_IMG, 0);
    hll::shader::set_mat_4f(sprite_shader, U_PROJ, proj);

    auto render_info = hll::sprite_renderer::create_data(sprite_shader);

    // Load textures
    // -------------
    if (!hll::AssetManager::is_valid_path(BACKGROUND_PATH) ||
        !hll::AssetManager::is_valid_path(AWESOME_FACE_PATH) ||
        !hll::AssetManager::is_valid_path(BLOCK_PATH) ||
        !hll::AssetManager::is_valid_path(BLOCK_SOLID_PATH) ||
        !hll::AssetManager::is_valid_path(PADDLE_PATH))
    {
        std::cout << "Not all asset paths are valid!" << std::endl;
    }

    hll::AssetManager::load_tex_2d(BACKGROUND_PATH, false, BACKGROUND_NAME);
    hll::AssetManager::load_tex_2d(AWESOME_FACE_PATH, true, AWESOME_FACE_NAME);
    hll::AssetManager::load_tex_2d(BLOCK_PATH, false, BLOCK_NAME);
    hll::AssetManager::load_tex_2d(BLOCK_SOLID_PATH, false, BLOCK_SOLID_NAME);
    hll::AssetManager::load_tex_2d(PADDLE_PATH, true, PADDLE_PATH);

    // Init levels
    // -----------
    const auto w = width;
    const auto h = height / 2;
    GameLevel lvl_0;
    lvl_0.load(LVL_0_PATH, w, h);
    GameLevel lvl_1;
    lvl_1.load(LVL_1_PATH, w, h);
    GameLevel lvl_2;
    lvl_2.load(LVL_2_PATH, w, h);
    GameLevel lvl_3;
    lvl_3.load(LVL_3_PATH, w, h);

    std::vector levels = {
            std::move(lvl_0),
            std::move(lvl_1),
            std::move(lvl_2),
            std::move(lvl_3)
    };

    // Init player
    // -----------
    const auto player_pos = glm::vec2(
            ((width / 2.0f) - (PLAYER_SIZE.x / 2.0f)),
            (height - PLAYER_SIZE.y));

    player_obj = new GameObject(
            player_pos,
            PLAYER_SIZE,
            hll::AssetManager::get_tex_2d(PADDLE_PATH),
            glm::vec3(1.0f),
            glm::vec2(0.0f));

    // Init ball
    // ---------
    glm::vec2 ball_pos = player_pos + glm::vec2(
            (PLAYER_SIZE.x / 2.0f - BALL_RADIUS),
            (-hll::game::BALL_RADIUS * 2.0f));
    ball_obj = new BallObject(
            ball_pos,
            BALL_RADIUS,
            INITIAL_BALL_VELOCITY,
            hll::AssetManager::get_tex_2d(hll::game::AWESOME_FACE_NAME));

    return new hll::game::GameData {
            .state = state,
            .width = width,
            .height = height,
            .levels = levels,
            .curr_level = 0,
            .render_info = render_info,
    };
}

void hll::game::process_input(const hll::game::GameData &game_data, float dt) {
    if (game_data.state != hll::game::GameState::GAME_ACTIVE) { return; }

    float vel = hll::game::PLAYER_SPEED * dt;

    if (game_data.keys[GLFW_KEY_A]) {
        if (player_obj->pos.x >= 0.0f) {
            player_obj->pos.x -= vel;

            if (ball_obj->is_stuck) {
                ball_obj->pos.x -= vel;
            }
        }
    }

    if (game_data.keys[GLFW_KEY_D]) {
        if (player_obj->pos.x <= (game_data.width - player_obj->size.x)) {
            player_obj->pos.x += vel;

            if (ball_obj->is_stuck) {
                ball_obj->pos.x += vel;
            }
        }
    }

    if (game_data.keys[GLFW_KEY_SPACE]) {
        ball_obj->is_stuck = false;
    }
}

void hll::game::update(float dt, GameData &game_data) {
    ball_obj->move(dt, game_data.width);
    hll::game::handle_collisions(game_data.get_curr_lvl().bricks);

    // Check if ball reached bottom edge
    if (ball_obj->pos.y >= game_data.height) {
        hll::game::reset_lvl(game_data.get_curr_lvl());
        hll::game::reset_ply(game_data.width, game_data.height);
    }
}

void hll::game::render(const hll::game::GameData &game_data) {
    if (game_data.state != GameState::GAME_ACTIVE) { return; }

    hll::sprite_renderer::draw_sprite(
            game_data.render_info,
            hll::AssetManager::get_tex_2d(BACKGROUND_NAME),
            glm::vec2(0.0f, 0.0f),
            glm::vec2(game_data.width, game_data.height),
            0.0f,
            glm::vec3(1.0f));

    // render level
    game_data.get_curr_lvl().draw(game_data.render_info);

    // render player
    player_obj->draw(game_data.render_info);

    // render ball
    ball_obj->draw(game_data.render_info);
}

void hll::game::handle_collisions(std::vector<GameObject> &brick_objs) {
    const auto ball_center = ball_obj->pos + ball_obj->radius;

    // ball - brick collisions
    // -----------------------
    for (auto &brick : brick_objs) {
        if (brick.is_destroyed) { continue; }

        const auto col_res = hll::collider::check_collision_2d_circle_aabb(
                ball_center, ball_obj->radius,
                brick.pos, brick.size);

        // Skipp if not collided
        if (!std::get<0>(col_res)) { continue; }
        brick.is_destroyed = !brick.is_solid;

        const auto dir = std::get<1>(col_res);
        const auto diff = std::get<2>(col_res);

        const static auto PEN_MARGIN = 0.0f;

        // if horizontal
        if ((dir == hll::collider::Direction::LEFT) || (dir == hll::collider::Direction::RIGHT)) {
            ball_obj->vel.x = -ball_obj->vel.x;
            float penetration = ball_obj->radius - std::abs(diff.x) + PEN_MARGIN;
            ball_obj->pos.x += penetration * (dir == hll::collider::Direction::LEFT ? -1.0f : 1.0f);
        }
        // if vertical
        else {
            ball_obj->vel.y = -ball_obj->vel.y;
            float penetration = ball_obj->radius - std::abs(diff.y) + PEN_MARGIN;
            ball_obj->pos.y += penetration * (dir == hll::collider::Direction::UP ? 1.0f : -1.0f);
        }
    }

    // ball - paddle collisions
    // ------------------------
    if (!ball_obj->is_stuck) {
        const auto col_res = hll::collider::check_collision_2d_circle_aabb(
                ball_center, ball_obj->radius,
                player_obj->pos, player_obj->size);

        if (std::get<0>(col_res)) {
            const auto half_ply_width = player_obj->size.x / 2.0f;
            const auto paddle_center = player_obj->pos.x + half_ply_width;
            const auto dist = (ball_center.x - paddle_center);
            const auto percentage = dist / half_ply_width;

            const auto strength = 2.0f;
            const auto old_vel = ball_obj->vel;
            ball_obj->vel.x = hll::game::INITIAL_BALL_VELOCITY.x * percentage * strength;
//            ball_obj->vel.y = -ball_obj->vel.y;
            // assume ball always goes up (fix case when ball enters paddle)
            ball_obj->vel.y = -1.0f * std::abs(ball_obj->vel.y);
            ball_obj->vel = glm::normalize(ball_obj->vel) * glm::length(old_vel);
        }
    }
}

void hll::game::reset_lvl(GameLevel &curr_lvl) {
    curr_lvl.reload();
}

void hll::game::reset_ply(unsigned int width, unsigned int height) {
    player_obj->size = hll::game::PLAYER_SIZE;
    player_obj->pos = glm::vec2(
            (width / 2.0f - PLAYER_SIZE.x / 2.0f),
            (height - PLAYER_SIZE.y));

    ball_obj->reset(
            (player_obj->pos + glm::vec2((PLAYER_SIZE.x / 2.0f - hll::game::BALL_RADIUS) , -(hll::game::BALL_RADIUS * 2.0f))),
            hll::game::INITIAL_BALL_VELOCITY);
}
