# HeLLengine

## Structure

- **bin**: Contains compiled library file_path (DLL)
- **include**: Contains publicly distributed header files
- **lib**: Contains statically linkable files
- **doc**: Contains the documentation
- **build**: Contains build realted stuff
- **src**: Contains the source files and private headers
